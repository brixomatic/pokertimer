package de.plush.brix.pokertimer;

import static java.nio.file.StandardWatchEventKinds.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;

public class FileWatcher {
	private final ExecutorService daemon = Executors.newSingleThreadExecutor(runnable -> {
		final Thread t = Executors.defaultThreadFactory().newThread(runnable);
		t.setDaemon(true);
		return t;
	});

	protected List<FileListener> listeners = new ArrayList<>();
	protected final File folder;
	protected static final List<WatchService> watchServices = new ArrayList<>();

	@SafeVarargs
	public FileWatcher(final File folder, final FileListener... fileListeners) {
		this.folder = folder;
		listeners.addAll(List.of(fileListeners));
	}

	public FileWatcher start() {
		if (folder.exists()) {
			daemon.execute(this::run);
		}
		return this;
	}

	private void run() {
		try (WatchService watchService = FileSystems.getDefault().newWatchService()) {
			final Path path = Paths.get(folder.getAbsolutePath());
			path.register(watchService, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE);
			watchServices.add(watchService);
			boolean poll = true;
			while (poll) {
				poll = pollEvents(watchService);
			}
		} catch (IOException | InterruptedException | ClosedWatchServiceException e) {
			Thread.currentThread().interrupt();
		}
	}

	protected boolean pollEvents(final WatchService watchService) throws InterruptedException {
		final WatchKey key = watchService.take();
		final Path path = (Path) key.watchable();
		for (final WatchEvent<?> event : key.pollEvents()) {
			notifyListeners(event.kind(), path.resolve((Path) event.context()).toFile());
		}
		return key.reset(); // put key back into a ready state. If it returns false, the key is no longer valid and the watch loop can exit.
	}

	protected void notifyListeners(final WatchEvent.Kind<?> kind, final File file) {
		final FileEvent event = new FileEvent(file);
		if (kind == ENTRY_CREATE) {
			for (final FileListener listener : listeners) {
				listener.onCreated(event);
			}
			if (file.isDirectory()) {
				new FileWatcher(file).setListeners(listeners).start();
			}
		} else if (kind == ENTRY_MODIFY) {
			for (final FileListener listener : listeners) {
				listener.onModified(event);
			}
		} else if (kind == ENTRY_DELETE) {
			for (final FileListener listener : listeners) {
				listener.onDeleted(event);
			}
		}
	}

	public FileWatcher onCreated(final Predicate<FileEvent> pred, final Consumer<FileEvent> listener) {
		addListener(new FileAdapter() {
			@Override
			public void onCreated(final FileEvent event) {
				if (pred.test(event)) {
					listener.accept(event);
				}
			}
		});
		return this;
	}

	public FileWatcher onCreated(final Consumer<FileEvent> listener) {
		return onCreated(e -> true, listener);
	}

	public FileWatcher onModified(final Predicate<FileEvent> pred, final Consumer<FileEvent> listener) {
		addListener(new FileAdapter() {
			@Override
			public void onModified(final FileEvent event) {
				if (pred.test(event)) {
					listener.accept(event);
				}
			}
		});
		return this;
	}

	public FileWatcher onModified(final Consumer<FileEvent> listener) {
		return onModified(e -> true, listener);
	}

	public FileWatcher onDeleted(final Predicate<FileEvent> pred, final Consumer<FileEvent> listener) {
		addListener(new FileAdapter() {
			@Override
			public void onDeleted(final FileEvent event) {
				if (pred.test(event)) {
					listener.accept(event);
				}
			}
		});
		return this;
	}

	public FileWatcher onDeleted(final Consumer<FileEvent> listener) {
		return onDeleted(e -> true, listener);
	}

	public FileWatcher addListener(final FileListener listener) {
		listeners.add(listener);
		return this;
	}

	public FileWatcher removeListener(final FileListener listener) {
		listeners.remove(listener);
		return this;
	}

	public List<FileListener> getListeners() {
		return listeners;
	}

	public FileWatcher setListeners(final List<FileListener> listeners) {
		this.listeners = listeners;
		return this;
	}

	public static List<WatchService> getWatchServices() {
		return Collections.unmodifiableList(watchServices);
	}

	public static class FileEvent extends EventObject {
		public FileEvent(final File file) {
			super(file);
		}

		public File getFile() {
			return (File) getSource();
		}
	}

	public interface FileListener extends EventListener {
		void onCreated(FileEvent event);

		void onModified(FileEvent event);

		void onDeleted(FileEvent event);
	}

	public static abstract class FileAdapter implements FileListener {
		@Override
		public void onCreated(final FileEvent event) {
			// no implementation provided
		}

		@Override
		public void onModified(final FileEvent event) {
			// no implementation provided
		}

		@Override
		public void onDeleted(final FileEvent event) {
			// no implementation provided
		}
	}

}