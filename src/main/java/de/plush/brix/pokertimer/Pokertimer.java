package de.plush.brix.pokertimer;

import static de.plush.brix.pokertimer.UtilExceptions.rethrowUnchecked;
import static java.lang.Integer.parseInt;
import static java.lang.Math.min;
import static java.time.LocalDateTime.now;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.nio.file.*;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Predicate;
import java.util.logging.*;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.sound.sampled.*;
import javax.swing.*;
import javax.swing.Timer;

import de.plush.brix.pokertimer.FileWatcher.FileEvent;

/**
 * @author Wanja Gayk
 */
@SuppressWarnings("serial")
public class Pokertimer extends JPanel {

	private static Logger LOGGER;

	private static Path userConfigFile;
	private static Path configDir;

	private static Path userSoundFile;

	static {
		LOGGER = Logger.getLogger(Pokertimer.class.getName());
		LOGGER.setLevel(Level.FINEST);
		Thread.setDefaultUncaughtExceptionHandler((thread, ex) -> LOGGER.log(Level.SEVERE, "uncaught Exception", ex));
		try {
			// safe way of logging even a config-dir-creation error: Have a file handler that is outside the config dir, and switch handler after config dir
			// was created:
			final Handler tempHandler = new FileHandler("%t/Pokertimer.log");
			tempHandler.setFormatter(new SimpleFormatter());
			LOGGER.addHandler(tempHandler);
			final String user_home = System.getProperty("user.home");

			configDir = Paths.get(user_home, ".Pokertimer");
			if (!Files.exists(configDir)) {
				Files.createDirectory(configDir);
			}

			final Handler configDirDirHandler = new FileHandler("%h/.Pokertimer/Pokertimer.log");
			configDirDirHandler.setFormatter(new SimpleFormatter());
			LOGGER.addHandler(configDirDirHandler);
			LOGGER.removeHandler(tempHandler);
		} catch (final Throwable e) {
			LOGGER.log(Level.SEVERE, "error in static initializer", e);
			throw new RuntimeException(e); // log+throw is usually an antipattern, but I want to let the VM deal with the error.
		}
		// NOTE: uncaughtExceptionHandler does not yet work here, for whatever reason.
	}

	private final ExecutorService soundThread = Executors.newCachedThreadPool();

	enum State {
		Initialized, Running, Paused
	}

	private State state = State.Initialized;

	private final Clock clock;
	private LocalDateTime startTime;
	private LocalDateTime pauseStart;

	private List<BlindLevel> levels;
	private int currentLevelIndex;
	// private Iterator<BlindLevel> levelItr;
	private BlindLevel currentLevel;

	private JLabel clockLabel;
	private JLabel actionLabel;
	private JLabel timeLabel;
	private JButton startButton;
	private JButton pauseButton;
	private JButton resumeButton;
	private JButton skipButton;
	private JButton resetButton;

	private Image background;

	private BufferedImage dimBackground;

	private JLabel previousActionLabel;

	private JLabel nextActionLabel;

	private JLabel headlineLabel;

	public Pokertimer() {
		clock = Clock.systemDefaultZone();
		reset();
		createPanel();
		new Timer(250, this::updateDisplay).start();

		final Predicate<FileEvent> isUserConfigFile = event -> userConfigFile.getFileName().endsWith(event.getFile().getName());
		new FileWatcher(configDir.toFile())//
				.onModified(isUserConfigFile, event -> reset())//
				.onModified(isUserConfigFile, event -> LOGGER.info("Modified: " + event.getFile().getPath() + " - resetting application"))//
				.start();
	}

	private void start() {
		startTime = now(clock);
		state = State.Running;
		beep();
		// System.out.println("start");
		updateDisplay(null);
		updateFocus();
	}

	private void pause() {
		pauseStart = now(clock);
		state = State.Paused;
		beep();
		updateDisplay(null);
		updateFocus();
		// System.out.println("pause");
	}

	private void resume() {
		final var pauseTimeMilis = pauseStart.until(now(clock), ChronoUnit.MILLIS);
		startTime = startTime.plus(pauseTimeMilis, ChronoUnit.MILLIS);
		state = State.Running;
		beep();
		updateDisplay(null);
		updateFocus();
		// System.out.println("endPause");
	}

	private void gotoNextLevel() {
		nextLevel().ifPresent(nextLevel -> {
			currentLevel = nextLevel;
			++currentLevelIndex;
			// System.out.println("next level" + currentLevel);
			startTime = now(clock);
			beep();
			updateDisplay(null);
			updateFocus();
		});
		if (currentLevel.duration == Duration.ZERO) {
			pause();
		}
	}

	private void updateFocus() {
		Stream.of(startButton, pauseButton, resumeButton, skipButton)//
				.filter(JButton::isVisible)//
				.findFirst()//
				.ifPresent(JButton::requestFocusInWindow);
	}

	private void reset() {
		initSoundFile();

		final var lines = readConfigFile();
		levels = lines.stream().map(String::trim)//
				.filter(s -> !s.isEmpty()).filter(s -> !s.startsWith("#"))//
				.map(BlindLevel::fromString).toList();

		currentLevelIndex = 0;
		currentLevel = levels.get(currentLevelIndex);
		startTime = null;
		state = State.Initialized;
	}

	private boolean hasNextLevel() {
		return nextLevel().isPresent();
	}

	private Optional<BlindLevel> previousLevel() {
		return currentLevelIndex > 0 ? Optional.of(levels.get(currentLevelIndex - 1)) : Optional.empty();
	}

	private Optional<BlindLevel> nextLevel() {
		return currentLevelIndex + 1 < levels.size() ? Optional.of(levels.get(currentLevelIndex + 1)) : Optional.empty();
	}

	private void updateLevel() {
		if (state == State.Running) {
			if (LocalDateTime.now(clock).isAfter(startTime.plus(currentLevel.duration))) {
				gotoNextLevel();
			}
		}
	}

	private void updateDisplay(@SuppressWarnings("unused") final ActionEvent e) {
		final var ldt = now(clock);

		final DateTimeFormatter localTime = ldt.getSecond() % 2 == 0 //
				// && state != State.Running //
				? DateTimeFormatter.ofPattern("HH:mm")//
				: DateTimeFormatter.ofPattern("HH.mm");
		clockLabel.setText(localTime.format(ldt));

		updateLevel();

		headlineLabel.setText(currentLevel.message.map(any -> " ").orElse("Blinds")); // single space so text has a height (relevant for layout)
		previousActionLabel.setText(previousLevel().map(BlindLevel::displayString).orElse(" ")); // single space so text has a height (relevant for layout)
		nextActionLabel.setText(nextLevel().map(BlindLevel::displayString).orElse(" ")); // single space so text has a height (relevant for layout)

		switch (state) {
		case Initialized -> {
			headlineLabel.setText(" ");
			actionLabel.setText("Play Poker?");
			timeLabel.setText(" "); // single space so text has a height (relevant for layout)
			nextActionLabel.setText(" "); // single space so text has a height (relevant for layout)
		}
		case Running -> {
			actionLabel.setText(currentLevel.displayString());
			timeLabel.setText("(" + (hasNextLevel() ? remainingTime(ldt) : "∞") + ")");
		}
		case Paused -> {
			headlineLabel.setText(" ");
			previousActionLabel.setText(" "); // single space so text has a height (relevant for layout)
			actionLabel.setText(currentLevel.message().orElse("Pause"));
			nextActionLabel.setText(" "); // single space so text has a height (relevant for layout)
			timeLabel.setText(" "); // single space so text has a height (relevant for layout)
		}
		default -> throw new IllegalArgumentException("Unexpected value: " + state);
		}

		startButton.setVisible(state == State.Initialized);
		pauseButton.setVisible(state == State.Running && currentLevel.message.isEmpty() && hasNextLevel());
		resumeButton.setVisible(state == State.Paused);
		skipButton.setVisible(state == State.Running && currentLevel.canBeSkipped && hasNextLevel());
		resetButton.setVisible(state != State.Initialized);
		revalidate();
		repaint();
	}

	private String remainingTime(final LocalDateTime ldt) {
		final var endLevel = startTime.plus(currentLevel.duration);
		final var restMinutesTrunctated = ldt.until(endLevel, ChronoUnit.MINUTES);
		final long restMinutesRound = ldt.until(endLevel.plusSeconds(30), ChronoUnit.MINUTES);
		// System.out.println(restMinutesTrunctated + " ~" + restMinutesRound);
		if (restMinutesTrunctated < 1) {
			final long restSeconds = ldt.until(endLevel, ChronoUnit.SECONDS);
			return restSeconds + "s";
		} else if (restMinutesRound < 60) {
			return restMinutesRound + "m";
		} else {
			final long restHoursRound = ldt.until(endLevel.plusSeconds(30), ChronoUnit.HOURS);
			final long restMinutesWithoutHours = restMinutesRound - restHoursRound * 60;
			return restHoursRound + "h" + (restMinutesWithoutHours == 0 ? "" : ":" + restMinutesWithoutHours + "m");
		}
	}

	@Override
	protected void paintComponent(final Graphics g) {
		// super.paintComponent(g);
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0, 0, getWidth(), getHeight());
		final var pw = 332;
		final var ph = 280;
		if (getWidth() < 1040 || getHeight() < 500) {
			g.drawImage(dimBackground, 0, getHeight() - ph, pw, ph, this);
			g.drawImage(dimBackground, getWidth() - pw, getHeight() - ph, pw, ph, this);
		} else {
			g.drawImage(background, 0, getHeight() - ph, pw, ph, this);
			g.drawImage(background, getWidth() - pw, getHeight() - ph, pw, ph, this);
		}
	}

	private void createPanel() {

		background = new ImageIcon(getClass().getResource("resources/chips.png")).getImage();
		dimBackground = withChangedBrightness(background, 0.5f);

		setLayout(new BorderLayout());

		headlineLabel = new JLabel("");
		clockLabel = new JLabel("");

		previousActionLabel = new JLabel("Foo");
		actionLabel = new JLabel("");
		nextActionLabel = new JLabel("Bar");

		timeLabel = new JLabel("");

		startButton = customizedButton("Start", "resources/start.png", this::start);
		pauseButton = customizedButton("Pause", "resources/pause.png", this::pause);
		resumeButton = customizedButton("Resume", "resources/start.png", this::resume);
		skipButton = customizedButton("Skip", "resources/skip.png", this::gotoNextLevel);
		resetButton = customizedButton("Reset", "resources/stop.png", this::reset);
		resetButton.setFocusable(false); // to avoid shitty accidents!
		ifKeyPressedForMsInvoke(resetButton, KeyEvent.VK_ESCAPE, 0, 3000, this::reset);

		final JPanel top = new JPanel();
		top.setLayout(new GridLayout(1, 3));
		top.add(new JLabel(""));
		top.add(headlineLabel);
		top.add(clockLabel);

		final JPanel pastFuture = new JPanel();
		pastFuture.setLayout(new BoxLayout(pastFuture, BoxLayout.Y_AXIS));
		pastFuture.add(previousActionLabel);
		pastFuture.add(nextActionLabel);

		final JPanel pastFuturePresent = new JPanel();
		pastFuturePresent.setLayout(new OverlayLayout(pastFuturePresent));

		pastFuturePresent.add(actionLabel);
		pastFuturePresent.add(pastFuture);

		final JPanel middle = new JPanel();
		middle.setLayout(new BoxLayout(middle, BoxLayout.Y_AXIS));
		middle.add(pastFuturePresent);
		middle.add(timeLabel);

		final JPanel bottom = new JPanel();
		bottom.add(startButton);
		bottom.add(pauseButton);
		bottom.add(resumeButton);
		bottom.add(skipButton);
		bottom.add(resetButton);

		add(top, BorderLayout.NORTH);
		add(middle, BorderLayout.CENTER);
		add(bottom, BorderLayout.SOUTH);

		rightX(clockLabel);
		centerX(actionLabel, timeLabel, previousActionLabel, nextActionLabel, headlineLabel);
		centerY(actionLabel);

		setColors(null, top, middle, bottom, pastFuture, pastFuturePresent); // just makes background transparent
		setColors(Color.BLACK, previousActionLabel, nextActionLabel);
		setColors(Color.LIGHT_GRAY, clockLabel, timeLabel, headlineLabel);
		setColors(new Color(0xff, 0xff, 0xff, 0xd0), actionLabel); // White with some transparency
		setColors(Color.WHITE, startButton, pauseButton, resumeButton, skipButton, resetButton);

		updateDisplay(null);

		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(final ComponentEvent e) {
				final int big = (int) min(e.getComponent().getWidth() / 9, e.getComponent().getHeight() / 7.6);
				final int med = (int) (big * 0.84);
				// final int small = (int) (big * 0.6);
				headlineLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, med));
				clockLabel.setFont(new Font(Font.MONOSPACED, Font.BOLD, med));

				previousActionLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, med));
				actionLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, big));
				nextActionLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, med));

				timeLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, med));
				updateDisplay(null);
			}
		});

		Toolkit.getDefaultToolkit().addAWTEventListener(e -> {
			if (e instanceof final MouseEvent me //
					&& me.getID() == MouseEvent.MOUSE_CLICKED && (me.isPopupTrigger() || SwingUtilities.isRightMouseButton(me))) {
				LOGGER.fine("show popup: " + e);
				new ConfigurePopup().show(me.getComponent(), me.getX(), me.getY());
			}
		}, AWTEvent.MOUSE_EVENT_MASK);

		Toolkit.getDefaultToolkit().addAWTEventListener(e -> {
			LOGGER.fine("" + e + " " + (e.getSource() instanceof final Component jc ? jc.getBounds() : ""));
		}, AWTEvent.WINDOW_EVENT_MASK | AWTEvent.WINDOW_STATE_EVENT_MASK | AWTEvent.ACTION_EVENT_MASK);
	}

	private void ifKeyPressedForMsInvoke(final JButton button, final int evt, final int mod, final int millis, final Runnable job) {
		button.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(evt, mod, false), evt + "" + mod + "KEY_DOWN");
		button.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(evt, mod, true), evt + "" + mod + "KEY_UP");

		button.getActionMap().put(evt + "" + mod + "KEY_DOWN", toAction(e -> {
			if (button.getClientProperty(evt + "" + mod + "START_TIME") == null) {
				button.putClientProperty(evt + "" + mod + "START_TIME", System.currentTimeMillis());
				button.putClientProperty(evt + "" + mod + "FOCUSABLE", button.isFocusable());
				button.putClientProperty(evt + "" + mod + "TEXT", button.getText());
				button.setFocusable(true);
				button.requestFocusInWindow();
			}
			final var start = (long) button.getClientProperty(evt + "" + mod + "START_TIME");
			final var rest = Math.max(0, (700 + start + millis - System.currentTimeMillis()) / 1000);
			button.setText(button.getClientProperty(evt + "" + mod + "TEXT") + " " + rest);
		}));
		button.getActionMap().put(evt + "" + mod + "KEY_UP", toAction(e -> {
			try {
				if (button.getClientProperty(evt + "" + mod + "START_TIME") instanceof final Long startTime) {
					final var holdDownTime = System.currentTimeMillis() - startTime;
					if (holdDownTime > millis) {
						job.run();
					}
				}
			} finally {
				button.putClientProperty(evt + "" + mod + "START_TIME", null);
				if (button.getClientProperty(evt + "" + mod + "FOCUSABLE") instanceof final Boolean focusable) {
					button.setFocusable(focusable);
				}
				if (button.getClientProperty(evt + "" + mod + "TEXT") instanceof final String text) {
					button.setText(text);
				}
				updateFocus();
			}
		}));
	}

	private static Action toAction(final ActionListener e) {
		return new AbstractAction() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				e.actionPerformed(evt);
			}
		};
	}

	private static void centerX(final JComponent... components) {
		for (final JComponent comp : components) {
			if (comp instanceof final JLabel label) {
				label.setHorizontalAlignment(SwingConstants.CENTER); // works for GridLayout(0,1)
			}
			comp.setAlignmentX(Component.CENTER_ALIGNMENT); // works for BoxLayout
		}
	}

	private static void centerY(final JComponent... components) {
		for (final JComponent comp : components) {
			if (comp instanceof final JLabel label) {
				label.setVerticalAlignment(SwingConstants.CENTER); // works for GridLayout(0,1)
			}
			comp.setAlignmentY(Component.CENTER_ALIGNMENT); // works for BoxLayout
		}
	}

	private static void rightX(final JComponent... components) {
		for (final JComponent comp : components) {
			if (comp instanceof final JLabel label) {
				label.setHorizontalAlignment(SwingConstants.RIGHT); // works for GridLayout(0,1)
			}
			comp.setAlignmentX(Component.RIGHT_ALIGNMENT); // works for BoxLayout
		}
	}

	private static void setColors(final Color fg, final JComponent... components) {
		for (final var c : components) {
			c.setBackground(Color.DARK_GRAY);
			if (fg != null) {
				c.setForeground(fg);
			}
			c.setOpaque(false);
		}
	}

	private JButton customizedButton(final String text, final String iconName, final Runnable action) {
		final var icon = new ImageIcon(getClass().getResource(iconName));
		final var focusIcon = withChangedBrightness(icon, 1.2f);
		final var rolloverIcon = withChangedBrightness(icon, 1.5f);

		final var button = new JButton() {
			@Override
			protected void paintComponent(final Graphics g) {
				if (hasFocus() && getIcon() != focusIcon) {
					setIcon(focusIcon);
				} else if (!hasFocus() && getIcon() != icon) {
					setIcon(icon);
				}
				super.paintComponent(g);
			}
		};
		button.addActionListener(e -> action.run());
		button.setIcon(icon);
		button.setRolloverIcon(rolloverIcon);
		button.setText(text);
		button.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 24));
		button.setVerticalTextPosition(SwingConstants.BOTTOM);
		button.setHorizontalTextPosition(SwingConstants.CENTER);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		button.setBorder(null);
		button.setFocusable(true);
		button.setFocusPainted(false);
		return button;
	}

	private static ImageIcon withChangedBrightness(final ImageIcon icon, final float factor) {
		return new ImageIcon(withChangedBrightness(icon.getImage(), factor));
	}

	private static BufferedImage withChangedBrightness(final Image original, final float factor) {
		final BufferedImage image = new BufferedImage(original.getWidth(null), original.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		image.getGraphics().drawImage(original, 0, 0, null);
		return new RescaleOp(factor, 0, null).filter(image, image);
	}

	private void beep() {
		soundThread.execute(() -> {
			try {
				try {
					playWav(userSoundFile);
				} catch (IOException | UnsupportedAudioFileException e) {
					playTone(1046.50f, 200, 0.5);
				}
			} catch (final LineUnavailableException e) {
				Toolkit.getDefaultToolkit().beep();
			}
		});
	}

	private static void playWav(final Path soundFile) throws IOException, UnsupportedAudioFileException, LineUnavailableException {
		final int BUFFER_SIZE = 128000;
		try (var audioStream = AudioSystem.getAudioInputStream(soundFile.toFile());
				var line = AudioSystem.getLine(new DataLine.Info(SourceDataLine.class, audioStream.getFormat()));
				final var sourceLine = (SourceDataLine) line;) {
			sourceLine.open(audioStream.getFormat());
			sourceLine.start();
			int nBytesRead = 0;
			final byte[] abData = new byte[BUFFER_SIZE];
			while (nBytesRead != -1) {
				nBytesRead = audioStream.read(abData, 0, abData.length);
				if (nBytesRead >= 0) {
					@SuppressWarnings("unused")
					final int nBytesWritten = sourceLine.write(abData, 0, nBytesRead);
				}
			}
			sourceLine.drain();
		}
	}

	private static void playTone(final float hz, final int msecs, final double vol) throws LineUnavailableException {
		final float SAMPLE_RATE = 8000f;
		final byte[] buf = new byte[1];
		final AudioFormat af = new AudioFormat(SAMPLE_RATE, // sampleRate
				8, // sampleSizeInBits
				1, // channels
				true, // signed
				false); // bigEndian
		try (SourceDataLine sdl = AudioSystem.getSourceDataLine(af)) {
			sdl.open(af);
			sdl.start();
			for (int i = 0; i < msecs * 8; i++) {
				final double angle = i / (SAMPLE_RATE / hz) * 2.0 * Math.PI;
				buf[0] = (byte) (Math.sin(angle) * 127.0 * vol);
				sdl.write(buf, 0, 1);
			}
			sdl.drain();
			sdl.stop();
		}
	}

	private static void initSoundFile() {
		try {
			userSoundFile = configDir.resolve("signal.wav");
			if (!Files.exists(userSoundFile) || Files.size(userSoundFile) == 0) {
				LOGGER.info("No sound file. Recreating default.");
				try (var original = Pokertimer.class.getResourceAsStream("resources/signal.wav")) {
					Files.copy(original, userSoundFile, StandardCopyOption.REPLACE_EXISTING);
				}
			}
		} catch (final IOException e) {
			LOGGER.log(Level.WARNING, "Can't copy sound file", e);
		}
	}

	private static List<String> readConfigFile() {
		try {
			userConfigFile = Paths.get(configDir.toString(), "blinds.config.txt");
			if (!Files.exists(userConfigFile) || Files.size(userConfigFile) < 5) {
				LOGGER.info("No config file. Recreating default.");
				final List<String> defaultConfigLines = readDefaultConfigFile();
				Files.write(userConfigFile, defaultConfigLines);
			}
			LOGGER.fine("Reading " + userConfigFile);
			return Files.readAllLines(userConfigFile);
		} catch (final IOException e) {
			return rethrowUnchecked(e);
		}
	}

	private static List<String> readDefaultConfigFile() throws IOException {
		final var lines = new ArrayList<String>();
		try (var is = Pokertimer.class.getResourceAsStream("resources/blinds.config.txt"); //
				var isr = new InputStreamReader(is); //
				var br = new BufferedReader(isr)) {
			String line;
			while ((line = br.readLine()) != null) {
				lines.add(line);
			}
		}
		return lines;
	}

	record BlindLevel(int smallBlind, int bigBlind, Duration duration, boolean canBeSkipped, Optional<String> message) {

		private static Pattern pattern = Pattern.compile("(\\d+)/(\\d+),(-?\\d+)(?:,([^#]*))?(?:\s*#.*)?");

		static BlindLevel fromString(final String config) {
			try {
				final var matcher = pattern.matcher(config);
				matcher.find();
				final var sb = parseInt(matcher.group(1));
				final var bb = parseInt(matcher.group(2));
				final var mins = parseInt(matcher.group(3));
				final var message = Optional.ofNullable(matcher.group(4)).map(String::trim).filter(s -> !s.isBlank());
				return new BlindLevel(//
						sb, //
						bb, //
						Duration.ofMinutes(Math.abs(mins)), //
						mins < 0, //
						message//
				);
			} catch (final Exception e) {
				LOGGER.log(Level.WARNING, "Can't parse Blindlevel '" + config + "'", e);
				return new BlindLevel(0, 0, Duration.ZERO, true, Optional.of("config error"));
			}
		}

		String displayString() {
			return message.orElse(smallBlind + " / " + bigBlind);
		}
	}

	private class ConfigurePopup extends JPopupMenu {
		public ConfigurePopup() {
			final JMenuItem menuItem = new JMenuItem("Configure");
			menuItem.setFont(new Font(Font.MONOSPACED, Font.BOLD, 24));
			menuItem.addActionListener(e -> {
				edit(userConfigFile.toFile());
			});
			add(menuItem);
		}

		public void edit(final File file) {
			try {
				if (Desktop.isDesktopSupported()) {
					final Desktop desktop = Desktop.getDesktop();
					if (!desktop.isSupported(Desktop.Action.EDIT)) {
						desktop.edit(file);
						return;
					}
				}
				final String cmd;
				if (System.getProperty("os.name").toLowerCase().contains("windows")) {
					cmd = "rundll32 url.dll,FileProtocolHandler " + file.getCanonicalPath();
				} else if (System.getProperty("os.name").toLowerCase().contains("linux")) {
					cmd = "xdg-open " + file.getCanonicalPath();
				} else if (System.getProperty("os.name").toLowerCase().contains("mac")) {
					cmd = "open " + file.getCanonicalPath();
				} else {
					return;
				}
				Runtime.getRuntime().exec(cmd).onExit();
			} catch (final IOException e) {
				LOGGER.log(Level.WARNING, "Can't open editor", e);
			}
		}
	}

	private static String monitorSizes() {
		final StringBuilder sb = new StringBuilder();
		final GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		for (final GraphicsDevice gd : ge.getScreenDevices()) {
			final var mode = gd.getDisplayMode();
			sb.append(gd.getIDstring()).append(": ")//
					.append(gd.getDefaultConfiguration().getBounds())//
					.append(mode.getBitDepth() > 0 ? mode.getBitDepth() + "bpp" : "[Multi depth]")//
					.append('@').append(mode.getRefreshRate() > 0 ? mode.getRefreshRate() + "Hz" : "[Unknown refresh rate]")//
					.append('\n');
		}

		return sb.toString();
	}

	public static void main(final String[] args) throws Exception {

		LOGGER.info('\n' + monitorSizes());

		final var icon = new ImageIcon(Pokertimer.class.getResource("resources/icon.png")).getImage();
		EventQueue.invokeAndWait(() -> {
			final JFrame frame = new JFrame("Wanjas Pokertimer");
			frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			frame.add(new Pokertimer());
			frame.setMinimumSize(new Dimension(500, 450));
			frame.setIconImage(icon);
			frame.pack();
			frame.setVisible(true);
			frame.setExtendedState(Frame.MAXIMIZED_BOTH);
		});
	}
}
