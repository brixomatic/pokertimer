# Pokertimer (c)2022 by Wanja Gayk

#  Format: 
#   sb/bb,minutes{,optional pause text}{# optional comment}
#   * Use 0 Minutes for pause until start-button is pressed.
#   * Use negative Minutes for a level or message that can be skipped.
#   * The last level will not show a count down (it's the last one anyway)
#  Examples:
#   Blinds 5/10 for 2 minutes:
#5/10,2
#   Blinds 5/10 for 2 minutes, can be skipped:
#5/10,-2
#   Display message and wait until "resume" is clicked:
#0/0,0,Raise for Chips
#   Display message for 5 minutes or until "skip" is pressed:
#0/0,-5,Raise for Chips
#   Display message for 5 minutes. Can not be paused and not be skipped: 
#0/0,5,Coffee Break

50/100,20
100/200,20
150/300,20
0/0,-5,Raise for Chips	#A five minutes pause that can be skipped.
200/400,15 
300/600,15 
400/800,15
500/1000,15
700/1500,15
1000/2000,15
1500/3000,15
2000/4000,15
2500/5000,15				#Last stage will go ad infinitum.

# Delete or rename this file to restore the defaults.
